#include <iostream>
#include <conio.h>
#include <fstream>
#include <cstdlib>
#include <ctime>
#include <windows.h>
#include <cmath>
#include <iomanip>
using namespace std;
int x[100],y[100];
int linLab[5]={33,-1,0,1,0},colLab[5]={44,0,1,0,-1};
int linCal[9]={33,2,1,-1,-2,-2,-1,1,2},colCal[9]={44,1,2,2,1,-1,-2,-2,-1};
int a[20][20];
fstream oture("rooks.txt",ios::out);
fstream oregine("queens.txt",ios::out);
fstream ocal("knight.txt",ios::out);
fstream olab("labyrinth.txt",ios::out);
fstream operm("permutations.txt",ios::out);
fstream oaranj("arrengements.txt",ios::out);
fstream osub("subsets.txt",ios::out);
int posibilBunPerm(int k)
{
    int i;
    for (i=1; i<k; i++)
        if (x[i]==x[k])
            return 0;
    return 1;
}
void solutiePerm(int n)
{
    int i;
    for (i=1; i<=n; i++)
        cout<<y[x[i]]<<" ";
    cout<<"\n";
}
void solutieFisPerm(int n)
{
    int i;
    for (i=1; i<=n; i++)
        operm<<y[x[i]]<<" ";
    operm<<"\n";
}
void solutieFisAranj(int n)
{
    int i;
    for (i=1; i<=n; i++)
        oaranj<<y[x[i]]<<" ";
    oaranj<<"\n";
}
void solutieFisSub(int n)
{
    int i;
    for (i=1; i<=n; i++)
        osub<<y[x[i]]<<" ";
    osub<<"\n";
}
void solutieFisTure(int n)
{
    int i,j;
    for (i=1; i<=n; i++)
    {
        for (j=1; j<=n; j++)
            if (x[i]==j)
                oture<<"T ";
            else
                oture<<"- ";
        oture<<"\n";
    }
    oture<<"\n";
}
void solutieTure(int n)
{
    int i,j;
    for (i=1; i<=n; i++)
    {
        for (j=1; j<=n; j++)
            if (x[i]==j)
                cout<<"T ";
            else
                cout<<"- ";
        cout<<"\n";
    }
    cout<<"\n";
}
void ture()
{
    int n,k=1,gasit,opt;
    HANDLE consola = GetStdHandle(STD_OUTPUT_HANDLE);
	SetConsoleTextAttribute(consola, 14);
    cout<<"Place n rooks on an n x n chessboard \nso that no two rooks attack each other.\n";
    SetConsoleTextAttribute(consola, 7);
    cout<<"Give the dimension of the chessboard : ";cin>>n;
    cout<<"1.Print the solution in rooks.txt\n2.Print the solution on the screen\n";
    cin>>opt;
    x[k]=0;
    while(k>0)
    {
        gasit=0;
        while (x[k]<n && gasit==0)
        {
            x[k]++;
            gasit=posibilBunPerm(k);
        }
        if (gasit==1)
            if (k<n)
            {
                k++;
                x[k]=0;
            }
            else
                if (opt==2)
                    solutieTure(n);
                else
                    solutieFisTure(n);
        else
            k--;
    }
}
void solutieFisRegine(int n)
{
    int i,j;
    for (i=1; i<=n; i++)
    {
        for (j=1; j<=n; j++)
            if (x[i]==j)
                oregine<<"R ";
            else
                oregine<<"- ";
        oregine<<"\n";
    }
    oregine<<"\n";
}
void solutieRegine(int n)
{
    int i,j;
    for (i=1; i<=n; i++)
    {
        for (j=1; j<=n; j++)
            if (x[i]==j)
                cout<<"R ";
            else
                cout<<"- ";
        cout<<"\n";
    }
    cout<<"\n";
}
int posibilBunRegine(int k)
{
    int i;
    for (i=1; i<k; i++)
        if (x[i]==x[k] || abs(x[i]-x[k])==k-i)
            return 0;
    return 1;
}
void regine()
{
    int n,k=1,gasit,opt;
    HANDLE consola = GetStdHandle(STD_OUTPUT_HANDLE);
	SetConsoleTextAttribute(consola, 14);
    cout<<"Place n queens on an n x n chessboard \nso that no two queens attack each other.\n";
    SetConsoleTextAttribute(consola, 7);
    cout<<"Give the dimension of the chessboard : ";cin>>n;
    cout<<"1.Print the solution in queens.txt\n2.Print the solution on the screen\n";
    cin>>opt;
    x[k]=0;
    while(k>0)
    {
        gasit=0;
        while (x[k]<n && gasit==0)
        {
            x[k]++;
            gasit=posibilBunRegine(k);
        }
        if (gasit==1)
            if (k<n)
            {
                k++;
                x[k]=0;
            }
            else
                if (opt==2)
                    solutieRegine(n);
                else
                    solutieFisRegine(n);
        else
            k--;
    }
}
void solutieCal(int n)
{
    int i,j;
    for (i=1; i<=n; i++)
    {
        for (j=1; j<=n; j++)
            cout<<setw(4)<<a[i][j]<<" ";
        cout<<"\n";
    }
    cout<<"\n";
}
void solutieFisCal(int n)
{
    int i,j;
    for (i=1; i<=n; i++)
    {
        for (j=1; j<=n; j++)
            ocal<<setw(4)<<a[i][j]<<" ";
        ocal<<"\n";
    }
    ocal<<"\n";
}
int posibilBunCal(int k, int &l, int &c, int n)
{
    int l1=l+linCal[x[k]],c1=c+colCal[x[k]];
    if (a[l1][c1]==0 && l1>=1 && l1<=n && c1>=1 && c1<=n)
    {
        a[l1][c1]=k+1;
        l=l1;
        c=c1;
        return 1;
    }
    return 0;
}
void cal()
{
    int n,gasit,opt,k=1,l,c;
    HANDLE consola = GetStdHandle(STD_OUTPUT_HANDLE);
	SetConsoleTextAttribute(consola, 14);
    cout<<"The knight is placed on the first block of an empty board and,\n moving according to the rules of chess, \nmust visit each square exactly once.\n";
    SetConsoleTextAttribute(consola, 7);
    cout<<"Give the dimension of the chessboard : ";
    cin>>n;
    opt=1;
    l=c=1;
    a[1][1]=1;
    cout<<"\n\n";
    cout<<"1.Print the solution in knight.txt\n2.Print the solution on the screen\n";
    cin>>opt;
    x[k]=0;
    while(k>0)
    {
        gasit=0;
        while (x[k]<8 && gasit==0)
        {
            x[k]++;
            gasit=posibilBunCal(k,l,c,n);
        }
        if (gasit==1)
            if (k==n*n-1)
            {
                if (opt==2)
                    solutieCal(n);
                else
                    solutieFisCal(n);
                x[++k]=8;
            }
            else
            {
                k++;
                x[k]=0;
            }
        else
        {
            a[l][c]=0;
            k--;
            l-=linCal[x[k]];
            c-=colCal[x[k]];
        }
    }

}
void solutieLab(int m, int n, int lp, int cp)
{
    int i,j;
    for (i=1; i<=m; i++)
    {
        for (j=1; j<=n; j++)
            if ((i==lp && j==cp) || a[i][j]>=2)
            {
                HANDLE consola = GetStdHandle(STD_OUTPUT_HANDLE);
                SetConsoleTextAttribute(consola, 14);
                cout<<setw(3)<<a[i][j]<<" ";
            }
            else
            {
                HANDLE consola = GetStdHandle(STD_OUTPUT_HANDLE);
                SetConsoleTextAttribute(consola, 7);
                cout<<setw(3)<<a[i][j]<<" ";
            }
        cout<<"\n";
    }
    cout<<"\n";
}
void solutieFisLab(int m, int n)
{
    int i,j;
    for (i=1; i<=m; i++)
    {
        for (j=1; j<=n; j++)
                olab<<setw(3)<<a[i][j]<<" ";
        olab<<"\n";
    }
    olab<<"\n";
}
int posibilBunLab(int k, int &l, int &c)
{
    int l1=l+linLab[x[k]],c1=c+colLab[x[k]];
    if (a[l1][c1]==0)
    {
        a[l1][c1]=k+1;
        l=l1;
        c=c1;
        return 1;
    }
    return 0;
}
int margine(int l, int c, int m, int n)
{
    return (l==m)||(c==n)||(l==1)||(c==1);
}
void labirint()
{
    int m,n,gasit,opt,k=1,l,c,i,j,lp,cp;
    HANDLE consola = GetStdHandle(STD_OUTPUT_HANDLE);
	SetConsoleTextAttribute(consola, 14);
    cout<<"Find all the ways out of labyrinth.\n";
    SetConsoleTextAttribute(consola, 7);
    cout<<"Give the number of rows : ";cin>>m;
    cout<<"Give the number of columns : ";cin>>n;
    opt=1;
    while(opt!=0)
    {
        cout<<"1.Generate the labyrinth randomly\n2.Give your own labyrinth\n";
        cin>>opt;
        switch(opt)
        {
            case 1 : srand(time(NULL));
                        for (i=1; i<=m; i++)
                            for (j=1; j<=n; j++)
                                a[i][j]=rand()%2;
                        opt=0;
                        break;
            case 2 : for (i=1; i<=m; i++)
                        for (j=1; j<=n; j++)
                        {
                            cout<<"a["<<i<<"]["<<j<<"]=";
                            cin>>a[i][j];
                        }
                    opt=0;
                    break;
            default : "Option not found!\n";opt=0;
        }
    }
    cout<<"Your labyrinth is :\n";
    for (i=1; i<=m; i++)
    {
        for (j=1; j<=n; j++)
            cout<<a[i][j]<<" ";
        cout<<"\n";
    }
    cout<<"Give the (row,column) to start : ";
    cin>>l>>c;
    lp=l; cp=c;
    a[l][c]=1;
    cout<<"\n\n";
    cout<<"1.Print the solution in labyrinth.txt\n2.Print the solution on the screen\n";
    cin>>opt;
    x[k]=0;
    while(k>0)
    {
        gasit=0;
        while (x[k]<4 && gasit==0)
        {
            x[k]++;
            gasit=posibilBunLab(k,l,c);
        }
        if (gasit==1)
            if (margine(l,c,m,n)==1)
            {
                if (opt==2)
                    solutieLab(m,n,lp,cp);
                else
                    solutieFisLab(m,n);
                x[++k]=4;
            }
            else
            {
                k++;
                x[k]=0;
            }
        else
        {
            a[l][c]=0;
            k--;
            l-=linLab[x[k]];
            c-=colLab[x[k]];
        }
    }
}
void permutari()
{
    int n,k=1,gasit,i,opt;
    cout<<"Give the number of elements of the set : ";cin>>n;
    opt=1;
    while(opt!=0)
    {
        cout<<"1.Generate the set randomly\n2.Give your own set\n";
        cin>>opt;
        switch(opt)
        {
            case 1 : srand(time(NULL));
                        for (i=1; i<=n; i++)
                            y[i]=rand()%100;
                        opt=0;
                        break;
            case 2 : for (i=1; i<=n; i++)
                        {
                            cout<<"y["<<i<<"]=";
                            cin>>y[i];
                        }
                    opt=0;
                    break;
            default : "Option not found!\n";opt=0;
        }
    }
    cout<<"Your set is :\n";
    for (i=1; i<=n; i++)
        cout<<y[i]<<" ";
    cout<<"\n\n";
    cout<<"1.Print the solution in permutations.txt\n2.Print the solution on the screen\n";
    cin>>opt;
    x[k]=0;
    while(k>0)
    {
        gasit=0;
        while (x[k]<n && gasit==0)
        {
            x[k]++;
            gasit=posibilBunPerm(k);
        }
        if (gasit==1)
            if (k<n)
            {
                k++;
                x[k]=0;
            }
            else
                if (opt==2)
                    solutiePerm(n);
                else
                    solutieFisPerm(n);
        else
            k--;
    }
}
void aranjamente()
{
    int n,k=1,gasit,i,opt,p;
    cout<<"Give the number of elements of the set : ";cin>>n;
    cout<<"Give the number of elements of the subset : ";cin>>p;
    opt=1;
    while(opt!=0)
    {
        cout<<"1.Generate the set randomly\n2.Give your own set\n";
        cin>>opt;
        switch(opt)
        {
            case 1 : srand(time(NULL));
                        for (i=1; i<=n; i++)
                            y[i]=rand()%100;
                        opt=0;
                        break;
            case 2 : for (i=1; i<=n; i++)
                        {
                            cout<<"y["<<i<<"]=";
                            cin>>y[i];
                        }
                    opt=0;
                    break;
            default : "Option not found!\n";opt=0;
        }
    }
    cout<<"Your set is :\n";
    for (i=1; i<=n; i++)
        cout<<y[i]<<" ";
    cout<<"\n\n";
    x[k]=0;
    cout<<"1.Print the solution in arrangements.txt\n2.Print the solution on the screen\n";
    cin>>opt;
    while(k>0)
    {
        gasit=0;
        while (x[k]<n && gasit==0)
        {
            x[k]++;
            gasit=posibilBunPerm(k);
        }
        if (gasit==1)
            if (k<p)
            {
                k++;
                x[k]=0;
            }
            else
                if (opt==2)
                    solutiePerm(p);
                else
                    solutieFisAranj(p);
        else
            k--;
    }
}
void submultimi()
{
    int n,k=1,gasit,i,opt,p;
    cout<<"Give the number of elements of the set : ";cin>>n;
    cout<<"Give the number of elements of the subset : ";cin>>p;
    opt=1;
    while(opt!=0)
    {
        cout<<"1.Generate the set randomly\n2.Give your own set\n";
        cin>>opt;
        switch(opt)
        {
            case 1 : srand(time(NULL));
                        for (i=1; i<=n; i++)
                            y[i]=rand()%100;
                        opt=0;
                        break;
            case 2 : for (i=1; i<=n; i++)
                        {
                            cout<<"y["<<i<<"]=";
                            cin>>y[i];
                        }
                    opt=0;
                    break;
            default : "Option not found!\n";opt=0;
        }
    }
    cout<<"Your set is :\n";
    for (i=1; i<=n; i++)
        cout<<y[i]<<" ";
    cout<<"\n\n";
    x[k]=0;
    cout<<"1.Print the solution in subsets.txt\n2.Print the solution on the screen\n";
    cin>>opt;
    while(k>0)
    {
        gasit=0;
        while (x[k]<n && gasit==0)
        {
            x[k]++;
            gasit=(x[k]>x[k-1]);
        }
        if (gasit==1)
            if (k<p)
            {
                k++;
                x[k]=0;
            }
            else
                if (opt==2)
                    solutiePerm(p);
                else
                    solutieFisSub(p);
        else
            k--;
    }
}
int main()
{
    int k,k1,k2;
    k=1;
    while (k!=0)
    {
        cout<<"1.Known problems\n2.Combinatorics problems\n0.Exit\n";
        k=getch(); k-=48;
        switch(k)
        {
            case 1 : cout<<"1.Rook problem\n2.Queen problem\n3.Knight problem\n4.Labyrinth\n";
                        cin>>k1;
                        switch(k1)
                        {
                            case 1 : ture();break;
                            case 2 : regine();break;
                            case 3 : cal();break;
                            case 4 : labirint();break;
                            default : cout<<"Optiune inexistenta!\n";
                        }break;
            case 2 : cout<<"1.Permutations of a set\n2.All the subsets with p elements of a set with n elements\n3.Arrangements with p elements of a set with n elements\n";
                        cin>>k2;
                        switch(k2)
                        {
                            case 1 : permutari();break;
                            case 2 : submultimi();break;
                            case 3 : aranjamente();break;
                            default : cout<<"Option not found!\n";
                        }break;
            case 0 : return 0; break;
            default : cout<<"Option not found!\n";
        }
    }
}
